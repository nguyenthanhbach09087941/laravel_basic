<?php 
namespace App\Repositories\Eloquent; 
 
use App\Repositories\Contracts\UserRepositoryInterface; 
 
class UserRepository implements UserRepositoryInterface 
{ 
	private $user; 
	public function __construct() { $this->user = new User();}
                 
	protected function dataUser($obj,$columnsSelected = array('*'))
        { 
            $argv = [
                 'id' => $obj->id,
            ];
            
            
             if (is_string($columnsSelected[0]) && $columnsSelected[0] != '*' ){
               // \Log::info('co chon cot');
                $newArgv = [];
                foreach ($columnsSelected as $selectCol){
                    $newArgv[$selectCol] = $argv[$selectCol];
                }
                $args = $newArgv;
            }
            else{
                
                $args = $argv;
            }
            return $args;
           
        } 
	protected function dataUsers($listData, $meta,$columnsSelected = array('*'))
        {
            $args = [];
            foreach ($listData as $obj) {
                $argv = [];
                $argv['id'] = $obj->id;
                if ($columnsSelected[0] != '*' ){
                $newArgv = [];
                foreach ($columnsSelected as $selectCol){
                        $newArgv[$selectCol] = $argv[$selectCol];
                    }
                    $args[] = $newArgv;
                }
                else{
                    $args[] = $argv;
                }
            }
            return ['items' => $args, 'meta' => $meta];
        }  
	public function model()
        {
                    return $this->user;
                        
        
        }  
	public function get($id,$columns = array('*'))
        {
                    $data = $this->user->find($id, $columns);
                        if ($data)
                        {
                            return $this->dataUser($data);
                        }
                        return null;
        
        }  
	public function all($columns = array('*'))
        {
            $listData = $this->user->get($columns);
            return $this->dataUsers($listData,null);
        }  
	public function paginate($perPage = 15,$columns = array('*'))
        {
            $listData = $this->user->paginate($perPage, $columns);
            return $this->dataUsers($listData,null);
        }  
	public function save(array $data) 
        {
        return $this->user->create($data);
            
        }  
	public function update(array $data,$id) {
         $dep =  $this->user->find($id);
        if ($dep)
        {
            foreach ($dep->getFillable() as $field)
            {
                if (array_key_exists($field,$data)){
                    $dep->$field = $data[$field];
                }
            }
            if ($dep->save())
            {
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        }  
	public function getByColumn($column,$value,$columnsSelected = array('*')) 
        {
        
             $data = $this->user->where($column,$value)->first();
            if ($data)
            {
                return $this->dataUser($data,$columnsSelected);
            }
            return null;
        
        
        }  
	public function getByMultiColumn(array $where,$columnsSelected = array('*')) 
        {
        
             $data = $this->user;
           
            foreach ($where as $key => $value) {
                $data = $data->where($key, $value);
            }
    
            $data = $data->first();
             
           
            if ($data)
            {
                return $this->dataUser($data,$columnsSelected);
            }
            return null;
        
        
        }  
	public function getListByColumn($column,$value,$columnsSelected = array('*')) 
        {
        
             $data = $this->user->where($column,$value)->get();
            if ($data)
            {
                return $this->dataUsers($data,null,$columnsSelected);
            }
            return null;
        
        
        }  
	public function getListByMultiColumn(array $where,$columnsSelected = array('*')) 
        {
        
             $data = $this->user;
             
              foreach ($where as $key => $value) {
            $data = $data->where($key, $value);
        }

        $data = $data->get();
        
            if ($data)
            {
                return $this->dataUsers($data,null,$columnsSelected);
            }
            return null;
        
        
        }  
	public function delete($id)
        {
            $del = $this->user->find($id);
            if ($del !== null)
            {
                $del->delete();
                return true;
            }
            else{
                return false;
            }
        } 
         
	public function deleteMulti(array $data)
        {
            $del = $this->user->whereIn("id",$data["list_id"])->delete();
            if ($del)
            {
              
                return true;
            }
            else{
                return false;
            }
        } 
         
} 