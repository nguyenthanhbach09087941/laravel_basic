<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    //
    private $user;
    public function __construct(UserRepositoryInterface $user)
    {
        $this->user = $user;
    }

    public function addUser(){
        return view('admin.user.add');
    }

    public function listUser(){
        return view('admin.user.list');
    }

    public function save(Request $request){
        $data = $this->user->save($request->all());
        var_dump($data);
        if ($data){
            return redirect()->to(route('admin.user.list'));
        }
        else{
            return redirect()->to(route('admin.user.add'));
        }
    }
}
