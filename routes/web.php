<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'Admin\DashBoardController@index')->name('admin');
Route::get('/admin/user/add', 'Admin\UserController@addUser')->name('admin.user.add');
Route::get('/admin/user/list', 'Admin\UserController@listUser')->name('admin.user.list');
Route::post('/admin/user/save', 'Admin\UserController@save')->name('admin.user.save');
